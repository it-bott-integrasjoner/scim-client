# scim-client

# Deprecated, use [scim2-client] (https://git.app.uib.no/it-bott-integrasjoner/scim2-client). 

Client to access SCIM, sebra version.
 
Currently SCIM at uib have gone from using "Mock Rapid Identity" to Sebra. The SCIM api 
with Sebra has some differences and this client is meant to be used. 

This client lib should be used rather than [rapid client](https://git.app.uib.no/it-bott-integrasjoner/rapid-client). 
 
[SCIM](https://git.app.uib.no/it-bott-integrasjoner/iam-scim)

## Commands

### Install
```sh
python setup.py install
```

### Unit tests

```sh
tox 
```
or  
```sh
python -m pytest
```

### Integration tests

Set credentials in `config.yaml` and run

```sh
python -m pytest -m integration
```
