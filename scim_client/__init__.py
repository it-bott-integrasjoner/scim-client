from .client import ScimClient, get_client
from .version import get_distribution


__all__ = ["ScimClient"]
__version__ = get_distribution().version
