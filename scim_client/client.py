import logging
import urllib.parse
from typing import Any, Dict, Iterator, Optional, Tuple, List, Iterable, Union
from types import ModuleType

import requests
from cachetools import cached, TTLCache

from scim_client.models import User, ScimClientConfig, UserRefs, Users

logger = logging.getLogger(__name__)

JsonType = Any


def urljoin(base_url: str, *paths: str) -> str:
    """An urljoin that takes multiple path arguments.

    Note how urllib.parse.urljoin will assume 'relative to parent' when the
    base_url doesn't end with a '/':

    >>> urllib.parse.urljoin('https://localhost/foo', 'bar')
    'https://localhost/bar'

    >>> urljoin('https://localhost/foo', 'bar')
    'https://localhost/foo/bar'

    >>> urljoin('https://localhost/foo', 'bar', 'baz')
    'https://localhost/foo/bar/baz'
    """
    for path in paths:
        base_url = urllib.parse.urljoin(base_url + "/", path)
    return base_url


def merge_dicts(*dicts: Optional[Dict[str, Any]]) -> Dict[str, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class ScimEndpoints:
    def __init__(self, url: str):
        if url.endswith("/"):
            url = url.rstrip("/")
        self.baseurl = url

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def get_user_url(self, user_id: str) -> str:
        return urljoin(self.baseurl, "Users", user_id)

    def get_users_url(self) -> str:
        return urljoin(self.baseurl, "Users")


class ScimClient:
    """Client for accessing SCIM"""

    default_headers: Dict[str, Any] = {}

    def __init__(
        self,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        rewrite_url: Optional[Tuple[str, str]] = None,
        use_sessions: Union[bool, requests.Session, None] = True,
    ):
        self.urls = ScimEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.rewrite_url = rewrite_url
        self.session: Union[requests.Session, ModuleType]
        if use_sessions:
            if isinstance(use_sessions, requests.Session):
                self.session = use_sessions
            else:
                self.session = requests.Session()
        else:
            self.session = requests

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: bool = False,
        auth: Optional[Tuple[str, str]] = None,
        **kwargs: Any,
    ) -> Union[JsonType, requests.Response, None]:
        if params is None:
            params = {}
        headers = merge_dicts(self.headers, headers)
        if self.rewrite_url is not None:
            url = url.replace(*self.rewrite_url)

        logger.debug("Calling url: %s", url)
        response = self.session.request(
            method_name, url, auth=auth, headers=headers, params=params, **kwargs
        )
        if return_response:
            return response
        else:
            if response.status_code in (500, 400):
                logger.warning("Got HTTP %d: %r", response.status_code, response.json())
            if response.status_code == 404:
                return None
            response.raise_for_status()
            return response.json()

    def put(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response, None]:
        return self.call("PUT", url, **kwargs)

    def get(
        self, url: str, params: Optional[Dict[str, Any]] = None, **kwargs: Any
    ) -> JsonType:
        return self.call(method_name="GET", url=url, params=params, **kwargs)

    def get_by_user_ref(self, user_resp: Dict[str, Any]) -> Optional[User]:
        user = None
        if user_resp:
            for user_ref in user_resp["Resources"]:
                user_id = user_ref["id"]
                user = self.get_by_id(user_id)
                break
        return user

    @cached(cache=TTLCache(maxsize=255, ttl=900))
    def get_by_id(self, id: str) -> Optional[User]:
        url = self.urls.get_user_url(id)
        resp = self.get(url)

        return User.from_dict(resp)

    def get_by_employee_nr(self, employee_nr: int) -> Optional[User]:
        url = self.urls.get_users_url()
        emp_nr_query = {"employeeNumber": str(employee_nr)}
        user_resp = self.get(url, params=emp_nr_query)

        user = self.get_by_user_ref(user_resp)

        if not user:
            logger.warning(f"Missing user, employee_nr: {employee_nr}")
        return user

    @cached(cache=TTLCache(maxsize=255, ttl=900))
    def get_by_username(self, username: str) -> Optional[User]:
        url = self.urls.get_users_url()
        username_query = {"userName": str(username)}
        user_resp = self.get(url, params=username_query)

        user = self.get_by_user_ref(user_resp)

        if not user:
            logger.warning(f"Missing user, userName: {username}")
        return user

    @cached(cache=TTLCache(maxsize=255, ttl=900))
    def get_by_fs_id(self, fs_id: int) -> Optional[User]:
        fs_query = {"fsPersonNummer": str(fs_id)}
        url = self.urls.get_users_url()
        user_resp = self.get(url, params=fs_query)
        user = self.get_by_user_ref(user_resp)

        if not user:
            logger.warning(f"Missing user, personlopenummer: {fs_id}")
        return user

    @cached(cache=TTLCache(maxsize=255, ttl=900))
    def get_by_nin(self, nin: str) -> Optional[User]:
        nin_query = {"norEduPersonNIN": nin}
        url = self.urls.get_users_url()
        user_resp = self.get(url, params=nin_query)
        user = self.get_by_user_ref(user_resp)
        if not user:
            logger.warning("Missing user, nin: %s", nin)
        return user

    def get_all_users(self, start_index: int = 1, count: int = 10) -> List[User]:
        user_refs = self.get_all_user_refs(start_index, count)
        if user_refs is None:
            return []

        result = []
        for user_ref in user_refs.Resources:
            user = self.get_by_id(user_ref.id)
            if user:
                result.append(user)
        return result

    def get_all_employees(self, count: Optional[int] = None) -> Iterator[List[User]]:
        """
        Get all users of type employee

        Includes all attributes necessary to create User objects straight away.
        Default count is 10 at the time of writing.

        returns a generator that yields count number of objects at a time
        """

        url = self.urls.get_users_url()
        params = {
            "userType": "Employee",
        }
        if count:
            params["count"] = str(count)

        resp: Dict[str, Any] = self.get(url, params=params)
        logging.debug(
            "[get_all_employees]: Got response: %s, type: %s", resp, type(resp)
        )

        resp_model = Users(**resp)
        yield resp_model.Resources
        for j in range(
            resp_model.items_per_page + 1,
            resp_model.total_results + 1,
            resp_model.items_per_page,
        ):
            params["startIndex"] = str(j)
            resp = self.get(url, params=params)
            resp_model = Users(**resp)
            yield resp_model.Resources

    @cached(cache=TTLCache(maxsize=255, ttl=900))
    def get_all_user_refs(
        self, start_index: int = 1, count: int = 10
    ) -> Optional[UserRefs]:
        """Get all user, with userRef"""
        url = self.urls.get_users_url()
        params = {"startIndex": start_index, "count": count}
        resp = self.get(url, params=params)
        logging.debug(f"[get_all_users]: Got response: {resp}," f" type:{type(resp)}")

        if resp is None:
            return None
        else:
            return UserRefs.from_dict(resp)


def get_client(config: ScimClientConfig) -> ScimClient:
    """
    Get a ScimClient from configuration.
    """
    return ScimClient(config.scim_client.base_url, headers=config.scim_client.headers)
