import json
from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Optional, TypeVar, Union

import pydantic
from pydantic import typing, Field

BaseModel_T = TypeVar("BaseModel_T", bound="BaseModel")


def to_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: typing.Type[BaseModel_T], data: Dict[str, Any]) -> BaseModel_T:
        return cls(**data)

    @classmethod
    def from_json(cls: typing.Type[BaseModel_T], json_data: str) -> BaseModel_T:
        data = json.loads(json_data)
        return cls.from_dict(data)


class ScimClientBase(BaseModel):
    base_url: pydantic.HttpUrl
    headers: typing.Dict[str, str] = {}
    use_sessions: bool = True


class ScimClientConfig(BaseModel):
    scim_client: ScimClientBase


class NameType(BaseModel):
    formatted: str
    family_name: Optional[str]
    given_name: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class UserMeta(BaseModel):
    resource_type: str
    created: datetime
    last_modified: datetime

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class EmailTypeEnum(str, Enum):
    work = "work"
    internal = "internal"


class EmailItem(BaseModel):
    value: str
    type: EmailTypeEnum


class PhoneNumberTypeEnum(str, Enum):
    work = "work"
    mobile = "mobile"


class PhoneNumberItem(BaseModel):
    value: str
    type: PhoneNumberTypeEnum


class RoleType(str, Enum):
    student = "Student"
    external = "External"
    system = "System"


class RoleItem(BaseModel):
    value: RoleType


class AccountType(str, Enum):
    primary = "primary"
    secondary = "secondary"
    admin = "admin"


class ScimUser(BaseModel):
    account_type: Union[AccountType, str, None]
    employee_number: Optional[int]
    student_number: Optional[int]
    fs_person_number: Optional[int]
    edu_person_principal_name: Optional[str]  # feide-id of this user
    user_principal_name: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class User(BaseModel):
    schemas: List[str]
    id: str
    meta: UserMeta
    userName: str
    external_id: Optional[str]
    name: Optional[NameType]
    display_name: Optional[str]
    profile_url: Optional[pydantic.HttpUrl]
    active: bool
    emails: Optional[List[EmailItem]]
    user_scim: ScimUser = Field(alias="no:edu:scim:user")

    # TODO: Not supported yet (roles,phoneNumbers)
    roles: Optional[List[RoleItem]]
    phone_numbers: Optional[List[PhoneNumberItem]]

    class Config:
        arbitrary_types_allowed = True
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class UserRef(BaseModel):
    id: str


class UserRefs(BaseModel):
    total_results: int
    # diff naming, not like others
    Resources: List[UserRef]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Users(BaseModel):
    total_results: int
    items_per_page: int
    start_index: int
    Resources: List[User]
    schemas: Optional[List[str]]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
