import json
import os

import pytest
from pathlib import Path
import yaml

from scim_client import get_client, models


@pytest.fixture
def ex_config():
    file = load_file("../config.example.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return models.ScimClientConfig(**configs)


@pytest.fixture
def service_config():
    file = load_file("../config.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return models.ScimClientConfig(**configs)


@pytest.fixture
def users_data():
    return load_json_file("fixtures/users.json")


@pytest.fixture
def employee_data():
    return load_json_file("fixtures/users_employees.json")


@pytest.fixture
def employee_data2():
    return load_json_file("fixtures/users_employees2.json")


@pytest.fixture
def user_ruth():
    return load_json_file("fixtures/user_ruth.json")


@pytest.fixture
def client(config: models.ScimClientConfig):
    return get_client(config)


@pytest.fixture
def client(ex_config):
    return get_client(ex_config)


def load_file(name, rb="r"):
    file = get_file(name)
    with open(file, rb) as fi:
        return fi.read()


def load_json_file(name, rb="r"):
    file = get_file(name)
    with open(file, rb) as fi:
        data = json.load(fi)
        return data


def get_file(name):
    print("loading file: ", name)
    file = os.path.join(os.path.dirname(__file__), name)
    file = Path(file)
    return file


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
