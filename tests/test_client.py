import pytest
import requests

from scim_client import models, ScimClient


@pytest.mark.parametrize(
    argnames=["use_sessions"],
    argvalues=[
        [True],
        [False],
        [None],
        [requests.Session()],
    ],
)
def test___init___use_sessions(use_sessions):
    client = ScimClient(
        url="https://localhost.localdomain/scim",
        use_sessions=use_sessions,
    )
    if use_sessions is True:
        assert isinstance(client.session, requests.Session)
    elif isinstance(use_sessions, requests.Session):
        assert client.session is use_sessions
    elif not use_sessions:
        assert client.session == requests
    else:
        assert False


def test_get_all_users(client, requests_mock, users_data):
    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users?"
        "startIndex=1&count=10",
        json=users_data,
    )

    received = client.get_all_user_refs(1, 10)
    expected = models.UserRefs.from_dict(users_data)
    assert expected == received


def test_get_by_user_name(client: ScimClient, requests_mock, user_ruth, users_data):
    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users"
        "?userName=ruped001@uib.no",
        json=users_data,
    )

    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users/1053", json=user_ruth
    )

    received = client.get_by_username("ruped001@uib.no")
    expected = models.User.from_dict(user_ruth)
    assert expected == received


def test_get_all_employees(client, requests_mock, employee_data, employee_data2):
    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users?userType=Employee&count=1",
        json=employee_data,
    )
    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users?userType=Employee&count=1&startIndex=2",
        json=employee_data2,
    )

    expected = [
        models.Users(**employee_data).Resources,
        models.Users(**employee_data2).Resources,
    ]
    received = [i for i in client.get_all_employees(count=1)]
    assert expected == received


def test_get_by_nin(client: ScimClient, requests_mock, user_ruth, users_data):
    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users"
        "?norEduPersonNIN=21015214462",
        json=users_data,
    )

    requests_mock.get(
        "https://gw-uib.intark.uh-it.no/iga/scim/sebra-test/Users/1053", json=user_ruth
    )

    received = client.get_by_nin("21015214462")
    expected = models.User.from_dict(user_ruth)
    assert expected == received
