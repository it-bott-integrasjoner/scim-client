import pytest

from scim_client import get_client
from scim_client.models import EmailTypeEnum


@pytest.mark.integration
def test_get_all_users(service_config):
    client = get_client(service_config)
    received = client.get_all_users()
    assert received is not None
    assert len(received) == 10


@pytest.mark.integration
def test_get_by_user_name_invalid(service_config):
    client = get_client(service_config)
    received = client.get_by_username("invalid-users")
    assert received is None


@pytest.mark.integration
def test_get_by_user_name_test_data(service_config):
    client = get_client(service_config)
    employee = client.get_by_username("rpe064")
    employee2 = client.get_by_username("can069")
    employee3 = client.get_by_username("mje072")
    employee4 = client.get_by_username("nte064")
    employee5 = client.get_by_username("ddu001")

    assert employee is not None
    assert employee2 is not None
    assert employee3 is not None
    assert employee4 is not None
    assert employee5 is not None

    assert employee.user_scim.employee_number is not None
    assert employee2.user_scim.employee_number is not None
    assert employee3.user_scim.employee_number is not None
    assert employee4.user_scim.employee_number is not None
    assert employee5.user_scim.employee_number is not None


@pytest.mark.integration
def test_get_by_employee_nr_not_found(service_config):
    client = get_client(service_config)
    employee = client.get_by_employee_nr(-1)
    assert employee is None


# TODO currently iga does not support employee_nr
@pytest.mark.integration
def test_get_by_employee_nr(service_config):
    client = get_client(service_config)
    employee = client.get_by_employee_nr(102160)
    assert employee is not None
    assert employee.display_name == "Ruth Pedersen"
    assert employee.userName == "rpe064@uib.no"
    assert len(employee.emails) == 1
    assert employee.emails[0].value == "Ruth.Pedersen@uibtest.no"
    assert employee.emails[0].type == EmailTypeEnum.work
    assert employee.user_scim.employee_number == 102160
    # TODO roles not supported at moment
    # assert len(employee.roles) ==None
    # assert employee.roles[0] == RoleType.staff
    # assert employee.roles[1] == RoleType.student
    # assert employee.phone_numbers is None
    # feide id
    assert employee.user_scim.edu_person_principal_name is not "ruped001@uib.no"


# TODO currently scim does not support fs_id
@pytest.mark.integration
def x_test_get_by_fs_id(service_config):
    client = get_client(service_config)
    employee = client.get_by_fs_id(100008)
    assert employee is not None
    assert employee.userName == "ruped001@uib.no"
    assert len(employee.roles) == 2
    assert employee.user_scim.fs_person_number == 67890
