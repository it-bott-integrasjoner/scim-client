from scim_client import models


def test_models_users(users_data):
    users = models.UserRefs(**users_data)

    assert users is not None
    assert users.total_results == 1
    assert len(users.Resources) == 1

    user_ref = users.Resources[0]
    assert user_ref.id == "1053"


def test_models_user(user_ruth):
    user = models.User(**user_ruth)

    assert user is not None
    assert user.id == "999998"
    assert user.userName == "ruped001@uib.no"
    assert user.name.formatted == "Ruth Pedersen"
    assert user.name.family_name == "Pedersen"
    assert user.name.given_name == "Ruth"
    assert user.display_name == "Ruth Pedersen"
    assert user.active is True
    # TODO
    # assert len(user.roles) == 1
    # assert user.roles[0].value == RoleType.student
    # assert_phone_nr(user)

    assert_email(user)
    assert_scim_user(user)


def assert_scim_user(user):
    assert user.user_scim.account_type == models.AccountType.primary
    assert user.user_scim.edu_person_principal_name == "ruped001@uib.no"
    assert user.user_scim.user_principal_name == "Ruth.Pedersen@uibtest.no"


def assert_phone_nr(user):
    work_phone = next(
        filter(lambda x: x.type == models.PhoneNumberTypeEnum.work, user.phone_numbers)
    )
    assert work_phone.value == "+4793123456"


def assert_email(user):
    work_email = next(
        filter(lambda x: x.type == models.EmailTypeEnum.work, user.emails)
    )
    assert work_email.value == "Ruth.Pedersen@uibtest.no"
